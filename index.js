REX.X3D = (function () {
	var obj = {};
	var engineTimer = null;

	radToDeg = (rad) => {
		return (360 / (2 * Math.PI)) * rad
	}

	degToRad = (deg) => {
		return (2 * Math.PI / 360) * deg
	}

	obj.setVertical = (angle_deg) => {
		var rotation = $('#vertical_rotation').attr('rotation').split(' ')
		rotation[3] = degToRad(-angle_deg+45);
		$('#vertical_rotation').attr('rotation', rotation.join(' '));
		$('#vertical_rotation_disp').text(String(radToDeg(-rotation[3]).toFixed(2)) + '°')
	}

	obj.setHorizontal = (angle_deg) => {
		var rotation = $('#horizontal_rotation').attr('rotation').split(' ')
		rotation[3] = degToRad(angle_deg);
		$('#horizontal_rotation').attr('rotation', rotation.join(' '));
		$('#horizontal_rotation_disp').text(String(radToDeg(rotation[3]).toFixed(2)) + '°')
	}

	obj.enginesOn = () => {
		engineTimer = setInterval(function () {
			var rotation = $('#helicopter__Propeller_2_TRANSFORM').attr('rotation').split(' ')
			rotation[3] = parseFloat(rotation[3]) - 0.1
			$('#helicopter__Propeller_2_TRANSFORM').attr('rotation', rotation.join(' '));

			var rotation = $('#helicopter__Propeller_TRANSFORM').attr('rotation').split(' ')
			rotation[3] = parseFloat(rotation[3]) - 0.1
			$('#helicopter__Propeller_TRANSFORM').attr('rotation', rotation.join(' '));
		}, 1);
	}
	obj.enginesOff = () => clearInterval(engineTimer)

	return obj;
})();

// Funkce je volána po načtení dokumentu více viz jQuery
$(document).ready(function () {
	REX.HMI.setWsIP('ws://127.0.0.1:8008/rex');  

	var dataGroup = REX.WS.Group('dataGroup', 100);
	var controlGroup = REX.WS.Group('controlGroup', 0);

	REX.HMI.init = function() {
		dataGroup.addItem('GET_HORIZONTAL', 'vrtulnik.TRENDS1:ANGLE_hor'); // Vertical angle
		dataGroup.addItem('GET_VERTICAL', 'vrtulnik.TRENDS1:ANGLE_vert') // Horizontal angle

		controlGroup.addItem('SET_VERTICAL', 'vrtulnik.demos.CNR_SP_hor:ycn'); // Set vertical movement
		controlGroup.addItem('SET_HORIZONTAL', 'vrtulnik.demos.CNR_SP_vert'); // Set horizontal movement
		controlGroup.addItem('ENGINES', 'vrtulnik.CNB_ENABLE:YCN'); // Enable engine (true/false)
		controlGroup.addItem('REGULATOR', 'vrtulnik.CNB_man:YCN'); // Disable regulator (true/false)
		controlGroup.addItem('DEMO', 'vrtulnik.CNB_DEMO1:YCN'); // Enable demonstration mode

		dataGroup.on('read', function (group) {
		    //console.log(group)
			REX.X3D.setHorizontal(group.items[0].value)
			REX.X3D.setVertical(group.items[1].value)
		});
		
		dataGroup.on('add', function(group) {
			//console.log(group);
            group.read();
        });
		
		controlGroup.on('read', function (group) {
			if(group.items[2].value == 1) {
				REX.X3D.enginesOn();
				$("#engines_switch").attr('checked', 'checked');
			} else {
				REX.X3D.enginesOff();
			}
			if(group.items[4].value == 1) {
				$("#demo_switch").attr('checked', 'checked');
			}
		});
		
		controlGroup.on('add', function(group) {
			//console.log(group);
			REX.HMI.wsc.readGroup(group);
        });

		REX.HMI.addGroup(dataGroup);
        REX.HMI.addGroup(controlGroup);
	};

	const enableEngines = () => {
		REX.X3D.enginesOn();
		console.log(controlGroup.$i('ENGINES'));
		controlGroup.$i('ENGINES').setValue(1, true);
		controlGroup.$i('REGULATOR').setValue(0,true);
	}

	const disableEngines = () => {
		REX.X3D.enginesOff();
		controlGroup.$i('ENGINES').setValue(0, true);
		controlGroup.$i('REGULATOR').setValue(1,true);
	}

	const enableDemo = () => {
		controlGroup.$i('DEMO').setValue(1,true);
	}

	const disableDemo = () => {
		controlGroup.$i('DEMO').setValue(0,true);
	}

	$("#engines_switch").change(function (event) {
		var checkbox = event.target;
		if (checkbox.checked) {
			enableEngines();
			console.log('Engines enabled')
		} else {
			disableEngines();
			console.log('Engines disabled')
		}
	});

	$("#demo_switch").change(function (event) {
		var checkbox = event.target;
		if (checkbox.checked) {
			enableDemo()
			console.log('DEMO enabled')
		} else {
			disableDemo()
			console.log('DEMO disabled')
		}
	});

	$(".digital-display-btn").click(function () {
		let step = 5;
		var data = $(this).attr('data').split('-')
		var disp = null

		if(data[0] == 'h') {
			disp = $("#horizontal_rotation_set_disp")
		}

		if(data[0] == 'v') {
			disp = $("#vertical_rotation_set_disp")
		}

		var val = parseFloat(disp.text())

		if(data[1] == 'up') {
			val += step;
		}

		if(data[1] == 'down') {
			val -= step;
		}

		disp.text(`${val}°`);
	});

	$(".digital-display-set-btn").click(function () {
		var data = $(this).attr('data')
		if(data == 'h') {
			const disp = $("#horizontal_rotation_set_disp")
			controlGroup.$i("SET_HORIZONTAL").setValue(parseFloat(disp.text()), true);
		}

		if(data == 'v') {
			const disp = $("#vertical_rotation_set_disp")
			controlGroup.$i("SET_VERTICAL").setValue(parseFloat(disp.text()), true);
		}
	});

	REX.HMI.connect();
});